微信企业号 Java SDK
===============
当前最新版本： 1.0（发布日期：20160418）

前言：

1、jeewx-qywx-api为何诞生

    基于微信企业号的开发，虽然微信官方提供了相关接口文档，但是代码调用还是比较麻烦，为减轻开发者工作，捷微将微信企业号API进行统一封装，方便用户调用！


2、快速使用方法：

    如果要开发企业号应用，在你的maven项目中添加：

	<dependency>
	   <groupId>org.jeewx</groupId>
	   <artifactId>jeewx-qywx-api</artifactId>
	   <version>1.0-SNAPSHOT</version>
	   <type>jar</type>
	</dependency>

	
3、接口封装清单 : 

	1、获取access_token
	2、消息的加解密
	3、userid转换成openid接口的api
	4、openid转换成userid接口
	5、企业微信组织机构接口
	6、多媒体素材接口
	7、菜单接口
	8、群发消息接口
	9、用户接口
   
4、微信官方文档

* [官方文档](http://qydev.weixin.qq.com/wiki/index.php?title=%E9%A6%96%E9%A1%B)


5、更多资料

    为了大家更便捷的使用jeewx-qywx-api，社区制作相关的API文档和使用手册,更多资料请在论坛: www.jeecg.org 查询
   
   
6、关于Pull Request
   
    非常欢迎和感谢对本项目发起Pull Request的同学，对本项目感兴趣想参与的同学可以加QQ群：390507007